'use strict';

function set_scene(sc) {
			if (sc == 'scene_01') {
				var text = '暑いって言われても急にはさげれへんでー'; 
				var speaker = 'takeru'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'anger';
				var emotion_level = '2';
				var pitch = '80';
				var who = id_ganko;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_01_default') {
				var who = id_ganko;
				clients[who].send("scene_default");
			}
			else if (sc == 'scene_02') {
				var text = 'いいじゃない、いっぱい冷やして上げなよ'; 
				var speaker = 'hikari'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'happiness';
				var emotion_level = '4';
				var pitch = '100';
				var who = id_jyunan;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_02_default') {
				var who = id_jyunan;
				clients[who].send("scene_default");
			}
			else if (sc == 'scene_03') {
				var text = 'あかん！寒い人もいてるかもしれへんから温度は一定に。それがわしのポリシーや'; 
				var speaker = 'takeru'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'anger';
				var emotion_level = '2';
				var pitch = '80';
				var who = id_ganko;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_03_default') {
				var who = id_ganko;
				clients[who].send("scene_default");
			}
			else if (sc == 'scene_04') {
				// Nothing
			}
			else if (sc == 'scene_05') {
				var text = '出張お疲れ様です！ガンガン冷やしましょう'; 
				var speaker = 'hikari'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'happiness';
				var emotion_level = '4';
				var pitch = '100';
				var who = id_jyunan;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_05_default') {
				var who = id_jyunan;
				clients[who].send("scene_default");
			}
			else if (sc == 'scene_06') {
				var text = 'そんなやさしくしたらつけあがるで'; 
				var speaker = 'takeru'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'anger';
				var emotion_level = '2';
				var pitch = '80';
				var who = id_ganko;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_06_default') {
				var who = id_ganko;
				clients[who].send("scene_default");
			}
			else if (sc == 'scene_07') {
				var text = 'いいじゃないですか'; 
				var speaker = 'hikari'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'happiness';
				var emotion_level = '4';
				var pitch = '100';
				var who = id_jyunan;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_07_default') {
				var who = id_jyunan;
				clients[who].send("scene_default");
			}
			else if (sc == 'scene_08') {
				// Nothing
			}
			else if (sc == 'scene_09') {
				var text = 'はい、お疲れ様ですー！ガンガン下げますよ'; 
				var speaker = 'hikari'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'happiness';
				var emotion_level = '4';
				var pitch = '100';
				var who = id_jyunan;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_09_default') {
				var who = id_jyunan;
				clients[who].send("scene_default");
			}
			else if (sc == 'scene_10') {
				// Nothing
			}
			else if (sc == 'scene_11') {
				var text = 'あっちはコロコロ温度が変わって落ち着かへんな。こっちは一定温度で快適やで。'; 
				var speaker = 'takeru'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'anger';
				var emotion_level = '2';
				var pitch = '80';
				var who = id_ganko;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_11_default') {
				var who = id_ganko;
				clients[who].send("scene_default");
			}
			else if (sc == 'scene_12') {
				var text = '男は黙って28度！'; 
				var speaker = 'takeru'; // show, haruka, hikari, takeru, santa, bear,
				var emotion = 'anger';
				var emotion_level = '4';
				var pitch = '150';
				var who = id_ganko;
				const spawn = require( 'child_process' ).spawnSync,
					ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/" + sc + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+text, '-d', "emotion="+emotion, '-d', "emotion_level="+emotion_level, '-d', "pitch="+pitch, '-d', "speaker="+ speaker] );
				clients[who].send(sc);
			}
			else if (sc == 'scene_12_default') {
				var who = id_ganko;
				clients[who].send("scene_default");
			}

}

var count = 0;
var id_ganko = 0;
var id_jyunan = 1;
var clients = {};
///////////////////// 
  var PubNub = require('pubnub');
  var pubnub = new PubNub({
    publishKey : "pub-c-9974be30-835d-4678-9e30-b011d1f40c30",
    subscribeKey : "sub-c-99afcad8-9dc4-11e6-96cb-02ee2ddab7fe",
    secretKey : "sec-c-N2NmYTA5YWUtMWIxMC00ZWU2LWIxMWMtNDU3ODE0M2QwODc0"
  });

  pubnub.addListener({
    status: function(statusEvent) {
      if (statusEvent.category === "PNConnectedCategory") {
        //publishSampleMessage();
      }
    },
    message: function(message) {
      console.log("New Message!!", message.message);
			var gp = message.message;
			if (gp == 'group_01') {
				set_scene("scene_01");
				setTimeout(function() {set_scene("scene_01_default");}, 4000);
				setTimeout(function() {set_scene("scene_02");}, 5000);
				setTimeout(function() {set_scene("scene_02_default");}, 9000);
				setTimeout(function() {set_scene("scene_03");}, 10000);
				setTimeout(function() {set_scene("scene_03_default");}, 14000);
				setTimeout(function() {set_scene("scene_04");}, 15000);
				setTimeout(function() {set_scene("scene_04_default");}, 20000);
			}
			else if (gp == 'group_02') {
				set_scene("scene_05");
				setTimeout(function() {set_scene("scene_05_default");}, 4000);
				setTimeout(function() {set_scene("scene_06");}, 5000);
				setTimeout(function() {set_scene("scene_06_default");}, 9000);
				setTimeout(function() {set_scene("scene_07");}, 10000);
				setTimeout(function() {set_scene("scene_07_default");}, 14000);
				setTimeout(function() {set_scene("scene_08");}, 15000);
				setTimeout(function() {set_scene("scene_08_default");}, 20000);
			}
			else if (gp == 'group_03') {
				set_scene("scene_09");
				setTimeout(function() {set_scene("scene_09_default");}, 4000);
				setTimeout(function() {set_scene("scene_10");}, 5000);
				setTimeout(function() {set_scene("scene_10_default");}, 9000);
				setTimeout(function() {set_scene("scene_11");}, 10000);
				setTimeout(function() {set_scene("scene_11_default");}, 17000);
				setTimeout(function() {set_scene("scene_12");}, 18000);
				setTimeout(function() {set_scene("scene_12_default");}, 25000);
			}

    },
    presence: function(presenceEvent) {
      // handle presence
    }
  })      
  console.log("Subscribing..");
  pubnub.subscribe({
    channels: ['DaikinHack'] 
  });

//////////////////////////////

var express = require("express");
var app = express();
var router = express.Router();
var path = __dirname + '/views/';

console.log("Server started");

// router.use(function (req,res,next) {
//   console.log("/" + req.method);
//   next();
// });

router.get("/",function(req,res){
  res.sendFile(path + "index.html");
});

router.get("/about",function(req,res){
  res.sendFile(path + "about.html");
});

router.get("/ganko",function(req,res){
  res.sendFile(path + "ganko.html");
});
router.get("/jyunan",function(req,res){
  res.sendFile(path + "jyunan.html");
});

router.get("/fetch_voice_wav_test",function(req,res){

	console.log( 'text is ' + req.query.text );
	console.log( 'sound_id is ' + req.query.sound_id );

	const
			spawn = require( 'child_process' ).spawnSync,
				ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "public/sounds/test_" + req.query.sound_id + ".wav", '-u' ,"axlmdkqd3ierr06j:", '-d', "text="+req.query.text, '-d', "speaker="+ req.query.speaker] );


	console.log( `stderr: ${ls.stderr.toString()}` );
	console.log( `stdout: ${ls.stdout.toString()}` );
  // res.sendFile(path + "contact.html");
	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify({ "sound_id": req.query.sound_id }));
});

var expressWs = require('express-ws')(app);
app.ws('/', function(ws, req) {
  ws.on('message', function(msg) {
    console.log(msg);
		clients[id_ganko].send(msg);
  });
	//var id = count++;
	clients[count++] = ws;
  //console.log('socket', req.testing);
	console.log('Connected!', count);
});

app.use("/",router);
app.use(express.static('public')); // Express provides a built-in middleware express.static to serve static files, such as images, CSS, JavaScript, etc.

//app.use("*",function(req,res){
//  res.sendFile(path + "404.html");
//});

app.on('request', request);

app.listen(3000,function(){
  console.log("Live at Port 3000");
});

//////////////////////////////
//var http = require('http');
//var server = http.createServer(function(request, response) {});
//server.listen(1234, function() {
//    console.log((new Date()) + ' Server is listening on port 1234');
//});
//var WebSocketServer = require("websocket").server,
//    http = require("http"),
//    server = http.createServer(app);
//
////var WebSocketServer = require('websocket').server;
//
//const wsServer = new WebSocketServer({
//    httpServer: server
//});
//var count = 0;
//var clients = {};
//wsServer.on('request', function(r){
//    // Code here to run on connection
//	var connection = r.accept('echo-protocol', r.origin);
//	// Specific id for this client & increment count
//	var id = count++;
//	// Store the connection method so we can loop through & contact all clients
//	clients[id] = connection
//	console.log((new Date()) + ' Connection accepted [' + id + ']');
//
//	// Create event listener
//	connection.on('message', function(message) {
//
//		// The string message that was sent to us
//		var msgString = message.utf8Data;
//
//		// Loop through all clients
//		for(var i in clients){
//			// Send a message to the client with the message
//			clients[i].sendUTF(msgString);
//		}
//
//	});
//
//	connection.on('close', function(reasonCode, description) {
//		delete clients[id];
//		console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
//	});
//});


///////////
function request(request, response) {
	    var store = '';

	    request.on('data', function(data) 
				    {
							        store += data;
							    });
	    request.on('end', function() 
				    {  console.log(store);
							        response.setHeader("Content-Type", "text/json");
							        response.setHeader("Access-Control-Allow-Origin", "*");
							        response.end(store)
							    });
	 }  
// function fetch_voice_wav() {
// 	const
// 			spawn = require( 'child_process' ).spawnSync,
// 				ls = spawn( 'curl', ["https://api.voicetext.jp/v1/tts", '-o', "test.wav", '-u' ,"YOUR_API_KEY:", '-d', "text=おはようございます", '-d', "speaker=hikari"] );
// }
